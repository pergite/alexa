﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alexa.Models
{
    public abstract class AlexaGWObject
    {
        public string Type;
        public string SeqNo;
    }

    public class AlexaGWRegister : AlexaGWObject
    {
        public string AppToken;

        public AlexaGWRegister()
        {
            this.Type = "Register";
        }
    }

    public class AlexaSlot
    {
        public string name;
        public string value;
    }

    public class AlexaIntent
    {
        public string name;
        public Dictionary<string, AlexaSlot> slots;
    }

    public class AlexaRequest : AlexaGWObject
    {
        public string sessionId;
        public AlexaIntent intent;
    }

    public class AlexaGWMessage : AlexaGWObject
    {
        public AlexaRequest Data;

        public AlexaGWMessage()
        {
            this.Type = "Request";
        }

        public AlexaGWResponse CreateResponse()
        {
            return new AlexaGWResponse()
            {
                SeqNo = this.SeqNo
            };
        }
    }

    public class AlexaPlainTextResponse
    {
        public string PlainText;
        public bool EndSession;

        public AlexaPlainTextResponse()
        {
            EndSession = true;
        }
    }

    public class AlexaGWResponse : AlexaGWObject
    {
        public AlexaPlainTextResponse Data;

        public AlexaGWResponse()
        {
            this.Type = "Response";
        }
    }

}
