﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using System.Xml;

using Starcounter;

using Alexa.Models;
using Alexa.Database;
using Alexa.Helpers;
using Alexa.Messaging;

namespace Alexa
{

    public enum ActionEnum { Unknown, Create, Availability, Book }

    public class AlexaSession
    {
        public ActionEnum Action;
        public string Calendar;
        public DateTime? From = null;
        public TimeSpan? Time = null;
        public TimeSpan? Duration = null;

        public DateTime Created;

        public AlexaSession()
        {
            Action = ActionEnum.Unknown;
            Created = DateTime.UtcNow;
        }
    }

    public class AlexaEngine
    {
        protected static ConcurrentDictionary<string, AlexaSession> sessions = new ConcurrentDictionary<string, AlexaSession>();

        public static AlexaGWResponse ProcessMessage(AlexaGWMessage message)
        {
            bool endSession = true;
            string plainText = "";
            var intent = message.Data.intent;
            var sessionId = message.Data.sessionId;

            DateTime now = DateTime.UtcNow;

            AlexaSession session;

            if (!sessions.TryGetValue(sessionId, out session))
                session = new AlexaSession();

            if (null != intent.slots)
            {
                if (intent.slots.HasValue("name"))
                {
                    session.Calendar = intent.slots["name"].value;
                }

                if (intent.slots.HasValue("date"))
                {
                    try
                    {
                        session.From = DateTime.ParseExact(intent.slots["date"].value, "yyyy-MM-dd", CultureInfo.CurrentCulture, DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AdjustToUniversal);
                    }
                    catch (Exception)
                    {
                        plainText = "I am sorry, but I did not understand what date";
                        goto exit;
                    }
                }

                if (intent.slots.HasValue("time"))
                {
                    var t = intent.slots["time"].value;

                    int hh = int.Parse(t.Substring(0, 2));
                    int mn = 0;

                    if (t.Length==5)
                        mn = int.Parse(t.Substring(3, 2));

                    session.Time = new TimeSpan(hh, mn, 0);
                }

                if (intent.slots.HasValue("duration"))
                {
                    session.Duration = XmlConvert.ToTimeSpan(intent.slots["duration"].value);
                }
            }

            if (intent.name == "Confirm")
            {
                switch (session.Action)
                {
                    case ActionEnum.Create:
                        {
                            Db.Transact(() =>
                            {
                                new Database.Calendar()
                                {
                                    Name = session.Calendar
                                };
                            });

                            plainText = $"Ok, new calendar {session.Calendar} was created. Do you want me to add a reservation for it?";
                            session.Action = ActionEnum.Book;
                            endSession = false;
                        }
                        break;

                    case ActionEnum.Book:
                        {
                            if (!session.From.HasValue)
                            {
                                plainText = "Allright, when do you want it?";
                                endSession = false;
                            }
                        }
                        break;

                    default:
                        plainText = "I am sorry, but I have no idea what you are talking about";
                        break;
                }
            }
            else if(intent.name == "Available")
            {
                session = new AlexaSession()
                {
                    Action = ActionEnum.Availability,
                    Calendar = session.Calendar
                };
            }
            else if (intent.name == "Book")
            {
                session.Action = ActionEnum.Book;
            }
            else if (intent.name == "DateTime")
            {
                // keep session!
            }

            switch(session.Action)
            {
                case ActionEnum.Availability:
                    {
                        endSession = false;

                        if (!session.From.HasValue)
                        {
                            plainText = $"What date and time should I check?";
                        }
                        else if (!session.Time.HasValue)
                        {
                            plainText = $"Right, {session.From.Value.ToString("yyyy-MM-dd")}. And what time?";
                        }
                        else if (!session.Duration.HasValue)
                        {
                            DateTime from = session.From.Value + session.Time.Value;
                            plainText = $"Got it, {from.ToString("yyyy-MM-dd HH:mm")}. And for how long?";
                        }
                        else
                        {
                            // got it all!
                            DateTime from = session.From.Value + session.Time.Value;
                            DateTime to = from + session.Duration.Value;

                            if (null != session.Calendar)
                            {
                                try
                                {
                                    if (CalendarHelper.CheckAvailability(session.Calendar, from, to))
                                    {
                                        plainText = $"Yes, conference room {session.Calendar} is available {from.ToString("yyyy-MM-dd")} at {from.ToString("HH:mm")}. Do you want me to make a reservation?";
                                        session.Action = ActionEnum.Book;
                                    }
                                    else
                                    {
                                        plainText = $"I am sorry, but conference room {session.Calendar} is already booked";
                                        endSession = true;
                                    }
                                }
                                catch (CalendarNotFoundException)
                                {
                                    plainText = $"I am sorry, but I could not find a conference room named {session.Calendar}. Do you want me to create it?";
                                    session.Action = ActionEnum.Create;
                                }
                            }
                            else
                            {
                                // check all calendars
                                var available = CalendarHelper.CheckAvailability(from, to);

                                if (0 == available.Count)
                                {
                                    plainText = $"I am sorry, but all conference rooms are booked";
                                    endSession = true;
                                }
                                else if (available.Count == 1)
                                {
                                    var c = available[0];

                                    plainText = $"Yes, conference room {c.Name} is available. Do you want me to reserve it for you?";
                                    session.Calendar = c.Name;
                                    session.Action = ActionEnum.Book;
                                }
                                else
                                {
                                    StringBuilder sb = new StringBuilder("Yes, conference ");

                                    sb.Append("rooms");

                                    int c = 1;
                                    foreach (var a in available)
                                    {
                                        if (c == 3 || c == available.Count)
                                        {
                                            if (available.Count > 1)
                                                sb.Append($" and");

                                            sb.Append($" {a.Name}");
                                            break;
                                        }
                                        else if (c > 1)
                                            sb.Append($", {a.Name}");
                                        else
                                            sb.Append($" {a.Name}");

                                        c++;
                                    }

                                    if (available.Count == 1)
                                        sb.Append(" is available.");
                                    else
                                        sb.Append(" are available.");

                                    plainText = sb.ToString();
                                    endSession = true;
                                }
                            }
                        }
                    }
                    break;

                case ActionEnum.Book:
                    {
                        if (null == session.Calendar)
                        {
                            plainText = $"Of course, which conference room?";
                        }
                        else if (!session.From.HasValue)
                        {
                            plainText = $"What date and time should I check?";
                        }
                        else if (!session.Time.HasValue)
                        {
                            plainText = $"Right, {session.From.Value.ToString("yyyy-MM-dd")}. And what time?";
                        }
                        else if (!session.Duration.HasValue)
                        {
                            DateTime from = session.From.Value + session.Time.Value;
                            plainText = $"Got it, {from.ToString("yyyy-MM-dd HH:mm")}. And for how long?";
                        }
                        else
                        {
                            // got it all!
                            DateTime from = session.From.Value + session.Time.Value;
                            DateTime to = from + session.Duration.Value;

                            try
                            {
                                if (CalendarHelper.Reserve(session.Calendar, from, to))
                                {
                                    plainText = $"Ok, conference room {session.Calendar} is booked {from.ToString("yyyy-MM-dd HH:mm")}";
                                    endSession = true;
                                }
                                else
                                {
                                    plainText = $"I am sorry, but conference room {session.Calendar} is already booked.";
                                    endSession = true;
                                }
                            }
                            catch (CalendarNotFoundException)
                            {
                                plainText = $"I am sorry, but I could not find a conference room named {session.Calendar}. Do you want me to create it for you?";
                                session.Action = ActionEnum.Create;
                                endSession = false;
                            }
                        }
                    }
                    break;

                default:
                    {
                        plainText = "I am sorry, but you completely lost me. Please start over";
                    }
                    break;
            }

        exit:

            if (!endSession)
                sessions[sessionId] = session;

            else if (sessions.ContainsKey(sessionId))
                sessions.TryRemove(sessionId, out AlexaSession dontCare);
                

            var resp = message.CreateResponse();
            resp.Data = new AlexaPlainTextResponse()
            {
                PlainText = plainText,
                EndSession = endSession
            };

            return resp;
        }
    }
}
