﻿using System;
using System.Threading;
using System.Net.Sockets;

using Starcounter;

using Alexa.Models;
using Alexa.Helpers;

using System.Threading.Tasks;
using System.Globalization;
using System.Collections.Generic;
using System.Collections.Concurrent;

using System.Text;

namespace Alexa.Messaging
{
    public class MessagingClient
    {
        protected const string SERVER_ENDPOINT = "alexor.pergite.com";
        //protected const string SERVER_ENDPOINT = "alexor.pergite.local.com";
        protected const int SERVER_PORT = 9997;

        protected static MessagingClient client = null;
        protected static object _lock = new object();

        protected bool askedToStop = false;
        protected Thread thread;
        protected ManualResetEvent reset;
        protected ManualResetEvent sendResponse;

        protected ConcurrentQueue<AlexaGWResponse> responses = new ConcurrentQueue<AlexaGWResponse>();

        protected string token;

        public static void Start(string token)
        {
            if (null == client)
            {
                lock(_lock)
                {
                    if(null==client)
                        client = new MessagingClient(token);
                }
            }
        }

        // Havent found any event to hook up to in order to detect if our Starcounter app is going down 
        // so the Stop() would currently never be called. Its still left inplace in case such an
        // event suddenly emerges
        public static void Stop()
        {
            if (null == client)
                throw new InvalidOperationException("MessagingClient not running");

            client.askedToStop = true;
            client.reset.Set();
        }

        public static void Reset()
        {
            if (null == client)
                throw new InvalidOperationException("MessagingClient not running");

            client.reset.Set();
        }

        public MessagingClient(string token)
        {
            reset = new ManualResetEvent(false);
            sendResponse = new ManualResetEvent(false);

            thread = new Thread(this.Reader);
            thread.Start();

            this.token = token;
        }

        public static void SendResponse(AlexaGWResponse response)
        {
            if (null == client)
                throw new InvalidOperationException("MessagingClient not running");

            client.responses.Enqueue(response);
            client.sendResponse.Set();
        }

        public void Reader()
        {
            Console.WriteLine("MessagingClient: started!");

            int backoff = 1;

            while (!askedToStop)
            {
                Console.WriteLine($"MessagingClient: Connecting to {SERVER_ENDPOINT}:{SERVER_PORT}");

                try
                {
                    using (TcpClient client = new TcpClient(SERVER_ENDPOINT, SERVER_PORT))
                    {
                        using (JsonSocketReaderWriter s = new JsonSocketReaderWriter(client.GetStream(), 128)) // readbuffersize=128, enough for one WCNotification
                        {
                            backoff = 1;

                            while (true)
                            {
                                // register us!
                                var register = new AlexaGWRegister();
                                register.AppToken = this.token;

                                Console.WriteLine(String.Format("MessagingClient: Registering AppToken [{0}]", register.AppToken));

                                s.SendJson(register);

                                reset.Reset();

                                // enter requests loop
                                while (true)
                                {
                                    var message = s.ReadJson<AlexaGWMessage>(reset);

                                    if (null == message)
                                    {
                                        if (reset.WaitOne(0))
                                        {
                                            // reregister (or quit if askedToStop)
                                            break;
                                        }
                                        else if(sendResponse.WaitOne(0))
                                        {
                                            AlexaGWResponse response;
                                            while(responses.TryDequeue(out response))
                                            {
                                                s.SendJson(response);
                                            }

                                            sendResponse.Reset();
                                        }
                                        else
                                        {
                                            // server disconnected us
                                            throw new ServerDisconnectedException();
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine($"MessagingClient: AlexaGWMessage");

                                        AlexaGWResponse resp=null;

                                        Scheduling.RunTask(() =>
                                        {
                                            resp = AlexaEngine.ProcessMessage(message);

                                        }).Wait();

                                        s.SendJson(resp);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (ServerDisconnectedException)
                {
                    if(backoff < 32)
                        backoff *= 2;

                    Console.WriteLine($"MessagingClient: Server disconnected us! Reconnecting in {backoff}s");
                    Thread.Sleep(backoff*1000);
                }
                catch(SocketException)
                {
                    if (backoff < 32)
                        backoff *= 2;

                    Console.WriteLine($"MessagingClient: Failed/lost connection to {SERVER_ENDPOINT}! Retrying in {backoff}s");
                    Thread.Sleep(backoff*1000);
                }
            }

            Console.WriteLine("MessagingClient: asked to stop, terminating gracefully");
        }
    }
}
