﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Starcounter;
using Alexa.Database;

namespace Alexa.Helpers
{
    public class CalendarNotFoundException : System.Exception
    {
    }

    public class Available
    {
        public string Name;
        public DateTime From;
    }

    public class CalendarHelper
    {
        public static bool CheckAvailability(string name)
        {
            return CheckAvailability(name, DateTime.UtcNow, DateTime.UtcNow.AddHours(2));
        }

        public static bool CheckAvailability(string name, DateTime from, DateTime to)
        {
            return CheckAvailability(name, from, to, out Calendar dontCare);
        }

        public static bool CheckAvailability(string name, DateTime from, DateTime to, out Calendar c)
        {
            c = Db.SQL<Calendar>("SELECT c FROM Alexa.Database.Calendar c WHERE c.Name=?", name).FirstOrDefault();

            if (c == null)
                throw new CalendarNotFoundException();

            return 0 == c.Events.Where(e => (e.BeginTime >= from && e.BeginTime <= to) || (e.EndTime >= from && e.EndTime <= to)).Count();
        }

        public static List<Available> CheckAvailability(DateTime from, DateTime to)
        {
            List<Available> available = new List<Available>();

            foreach( var c in Db.SQL<Calendar>("SELECT c FROM Alexa.Database.Calendar c") )
            {
                if (0 == c.Events.Where(e => (e.BeginTime >= from && e.BeginTime <= to) || (e.EndTime >= from && e.EndTime <= to)).Count())
                    available.Add(new Available() { Name = c.Name, From = from });
            }

            return available;
        }

        public static bool Reserve(string name, DateTime from, DateTime to)
        {
            Calendar c;
            if(CheckAvailability(name, from, to, out c))
            {
                Db.Transact(() => {

                    new Event()
                    {
                        Calendar = c,
                        BeginTime = from,
                        EndTime = to,
                        Name = "Booked by Alexa"
                    };
                });
                return true;
            }

            return false;
        }
    }
}
