﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Threading;

using Starcounter;
using Alexa.Models;

namespace Alexa.Helpers
{
    public static class Extensions
    {
        static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static string ISO8601(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddTHH:mm:ss.msZ");
        }

        public static DateTime FromUnixTime(this long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }

        public static long ToUnixTime(this DateTime date)
        {
            return Convert.ToInt64((date - epoch).TotalMilliseconds);
        }

        public static string UrlEncode(this string s)
        {
            return HttpUtility.UrlEncode(s);
        }

        public static int IndexOf(this StringBuilder sb, char c)
        {
            for(int i=0; i<sb.Length; i++)
            {
                if (sb[i] == c)
                    return i;
            }

            return -1;
        }

        public static Task WaitOneAsync(this WaitHandle waitHandle)
        {
            if (waitHandle == null)
                throw new ArgumentNullException("waitHandle");

            var tcs = new TaskCompletionSource<bool>();
            var rwh = ThreadPool.RegisterWaitForSingleObject(waitHandle,
                delegate { tcs.TrySetResult(true); }, null, -1, true);
            var t = tcs.Task;
            t.ContinueWith((antecedent) => rwh.Unregister(null));
            return t;
        }
       
        public static bool HasValue(this Dictionary<string, AlexaSlot> slots, string name)
        {
            AlexaSlot slot;
            if(slots.TryGetValue(name, out slot))
            {
                if (!String.IsNullOrEmpty(slot.value))
                    return true;
            }

            return false;
        }
    }
}
