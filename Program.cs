﻿using System;
using Starcounter;

using Alexa.Messaging;
using Alexa.Database;
using Alexa.ViewModels;

namespace Alexa
{
    class Program
    {
        static void Main()
        {
            Application.Current.Use(new HtmlFromJsonProvider());
            Application.Current.Use(new PartialToStandaloneHtmlProvider());

            Handle.GET("/Alexa", () =>
            {
                return new SettingsPage()
                {
                    Token = Settings.Get().Token
                };
            });

            MessagingClient.Start(Settings.Get().Token);
        }
    }
}
