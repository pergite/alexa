﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace Alexa.Database
{
    [Database]
    public class Settings
    {
        public string Token { get; set; }

        public static Settings Get()
        {
            var s = Db.SQL<Settings>("SELECT s FROM Alexa.Database.Settings s").FirstOrDefault();
            if(null==s)
            {
                Db.Transact(() =>
                {
                    s = new Settings()
                    {
                        Token = Guid.NewGuid().ToString("N")
                    };
                });
            }

            return s;
        }
    }
}
