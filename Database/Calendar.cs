﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Starcounter;

namespace Alexa.Database
{
    [Database]
    public class Calendar
    {
        public string Name { get; set; }

        public IEnumerable<Event> Events => Db.SQL<Event>("SELECT e FROM Alexa.Database.Event e WHERE e.Calendar=?", this);
    }

    [Database]
    public class Event
    {
        public Calendar Calendar { get; set; }

        public DateTime BeginTime { get; set; }
        public DateTime EndTime { get; set; }

        public string Name { get; set; }
    }
}
